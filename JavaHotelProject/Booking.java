/*
 * This class is responsible for maintaining booking information.
 */
public class Booking {
	private int bookingId;
	private int month;
	private int day;
	private int numDaysBooked;
	@SuppressWarnings("unused")
	private int numSingles;
	@SuppressWarnings("unused")
	private int numDoubles;
	@SuppressWarnings("unused")
	private int numTriples;
	private String hotelName;
public Booking() {
	this.bookingId = -1;
	this.month = -1;
	this.day = -1;
	this.numDaysBooked = 0;
	this.numSingles = 0;
	this.numDoubles = 0;
	this.numTriples = 0;
	this.hotelName = null;
}

public int getBookingId() {
	return bookingId;
}
public void setBookingId(int bookingId) {
	this.bookingId = bookingId;
}

public int getMonth() {
	return month;
}

public void setMonth(int month) {
	this.month = month;
}

public int getDay() {
	return day;
}

public void setDay(int day) {
	this.day = day;
}

public int getNumDaysBooked() {
	return numDaysBooked;
}

public void setNumDaysBooked(int numDaysBooked) {
	this.numDaysBooked = numDaysBooked;
}

public void setNumSingles(int numSingles) {
	this.numSingles = numSingles;
}

public void setNumDoubles(int numDoubles) {
	this.numDoubles = numDoubles;
}

public void setNumTriples(int numTriples) {
	this.numTriples = numTriples;
}

public String getHotelName() {
	return hotelName;
}

public void setHotelName(String hotelName) {
	this.hotelName = hotelName;
}
}
