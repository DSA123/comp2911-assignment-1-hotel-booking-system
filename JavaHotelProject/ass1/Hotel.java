import java.util.*;
/*
 * This class is responsible for maintaining a list of rooms,
 * taking booking requests from the system, and printing room vacancy.
 */
public class Hotel {
	private String name;
	private List<Room> rooms;
	private int numSingles;
	private int numDoubles;
	private int numTriples;
public Hotel(String name) {
	this.rooms = new ArrayList<Room>();
	this.name = name;
	this.numSingles = 0;
	this.numDoubles = 0;
	this.numTriples = 0;
}
/**
 * Takes in a request to add a room and tries to fulfill it.
 * @precondition roomNumber >= 0
 * @precondition size >= 1 && size <= 3 
 * @param roomNumber The number of the room
 * @param size The size of the room (single, double or triple)
 */
public void addRoom (int roomNumber, int size) {
	Room newRoom = new Room(roomNumber, size);
	rooms.add(newRoom);
	switch(size) {
		case 1: this.numSingles++;
		break;
		case 2: this.numDoubles++;
		break;
		case 3: this.numTriples++;
		break;
	}
}
/**
 * Checks the vacancy of the hotel and its rooms.
 * @precondition singlesRequested >= 0
 * @precondition doublesRequested >= 0
 * @precondition triplesRequested >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @param singlesRequested The number of single rooms requested
 * @param doublesRequested The number of double rooms requested
 * @param triplesRequested The number of triple rooms requested
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 * @return True if hotel has enough vacancies to fulfill request
 */
public boolean checkVacancy(int singlesRequested, int doublesRequested, int triplesRequested, int month, int day, int numDaysBooked) {
	boolean vacant = false;
	if (this.numSingles >= singlesRequested && this.numDoubles >= doublesRequested && this.numTriples >= triplesRequested) {
		for (Room a: rooms) {
			if (singlesRequested > 0) {
				if (a.getSize() == 1) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						singlesRequested--;
					}
				}
			}
			if (doublesRequested > 0) {
				if (a.getSize() == 2) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						doublesRequested--;
					}
				}
			}
			if (triplesRequested > 0) {
				if (a.getSize() == 3) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						triplesRequested--;
					}
				}
			}
			if (singlesRequested == 0 && doublesRequested == 0 && triplesRequested == 0) {
				vacant = true;
				break;
			}
		}
	}
	return vacant;
}
/**
 * Attempts to book rooms to fulfill the request.
 * @precondition singlesRequested >= 0
 * @precondition doublesRequested >= 0
 * @precondition triplesRequested >= 0
 * @precondition bookingId >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @param singlesRequested The number of single rooms requested
 * @param doublesRequested The number of double rooms requested
 * @param triplesRequested The number of triple rooms requested
 * @param bookingId The ID of the booking.
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 * @return True if all rooms requested are successfully booked
 */
public boolean bookRooms(int singlesRequested, int doublesRequested, int triplesRequested, int bookingId, int month, int day, int numDaysBooked) {
	boolean bookingStatus = false;
		for (Room a: rooms) {
			if (singlesRequested > 0) {
				if (a.getSize() == 1) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						a.setRoomVacancy(bookingId, month, day, numDaysBooked);
						singlesRequested--;
					}
				}
			}
			if (doublesRequested > 0) {
				if (a.getSize() == 2) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						a.setRoomVacancy(bookingId, month, day, numDaysBooked);
						doublesRequested--;
					}
				}
			}
			if (triplesRequested > 0) {
				if (a.getSize() == 3) { // single
					if (a.checkRoomVacancy(month, day, numDaysBooked)) {
						a.setRoomVacancy(bookingId, month, day, numDaysBooked);
						triplesRequested--;
					}
				}
			}
			if (singlesRequested == 0 && doublesRequested == 0 && triplesRequested == 0) {
				bookingStatus = true;
				break;
			}
		}
	return bookingStatus;
}
/**
 * Finds any room that contains a booking for a specific bookingId.
 * @precondition bookingId >= 0
 * @param bookingId	The ID of the booking.
 */
public void getBookedRooms(int bookingId) {
	for (Room a: rooms) {
		a.findBooking(bookingId);
	}
}
/**
 * Finds any room that contains a specific bookingId and sets it as -1 (vacant)
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @precondition bookingId >= 0
 * @param bookingId The ID of the booking being deallocated
 * @param month The month booked
 * @param day The first day booked
 * @param numDaysBooked The total number of days being booked
 */
public void deAllocateRooms(int month, int day, int numDaysBooked, int bookingId) {
	for (Room a: rooms) {
		a.deallocateRoom(month, day, numDaysBooked, bookingId);
	}
}
/**
 * Prints out the hotel's name and any bookings it contains.
 */
public void printHotel () {
	for (Room a: rooms) {
		System.out.print(this.name + " ");
		a.printRoomVacancy();
		System.out.println();
	}
}
public String getName() {
	return name;
}
}
