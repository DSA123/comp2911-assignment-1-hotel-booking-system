/*
 * @author David Dimitriadis
 * This class is responsible for keeping records of bookings,
 * maintaining the list of hotels and reading input from a text file.
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
public class HotelBookingSystem {
	private List<Hotel> hotels;
	private List<Booking> bookings;
public HotelBookingSystem() {
	this.hotels = new ArrayList<Hotel>();
	this.bookings = new LinkedList<Booking>();
}
/**
* Takes in an input file, uses a scanner to extract the relevant data and passes it
* into the HotelBookingSystem to fulfill request.
*
* @param args The file being passed in as input
* @exception  FileNotFoundException If there is no file found
*/
public static void main(String[] args) throws FileNotFoundException{
	Scanner scan = null;
	HotelBookingSystem hotelBookingSystem = new HotelBookingSystem();
	try {
		scan = new Scanner(new FileReader(args[0]));
		while(scan.hasNext()) {
			String firstWord = scan.next();
			if (firstWord.equals("Hotel")) {
				String hotelName = scan.next();
				int roomNumber = scan.nextInt();
				int size = scan.nextInt();
				boolean hotelExists = false;
				if (hotelBookingSystem.hotels != null) {
					for (Hotel a: hotelBookingSystem.hotels) {
						if (a.getName().equals(hotelName)) {
							hotelExists = true;
							break;
						}
					}
					if (hotelExists) {
						for (Hotel a: hotelBookingSystem.hotels) {
							if (a.getName().equals(hotelName)) {
								a.addRoom(roomNumber, size);
								break;
							}
						}
					} else {
						hotelBookingSystem.addHotel(hotelName);
						for (Hotel a: hotelBookingSystem.hotels) {
							if (a.getName().equals(hotelName)) {
								a.addRoom(roomNumber, size);
								break;
							}
						}
					}
				}
			} else if (firstWord.equals("Booking")) {
				int bookingId = scan.nextInt();
				String monthString = scan.next();
				int month = -1;
				if (monthString.equals("Jan")) { month = 0; }
				else if (monthString.equals("Feb")) { month = 1; }
				else if (monthString.equals("Mar")) { month = 2; }
				else if (monthString.equals("Apr")) { month = 3; }
				else if (monthString.equals("May")) { month = 4; }
				else if (monthString.equals("Jun")) { month = 5; }
				else if (monthString.equals("Jul")) { month = 6; }
				else if (monthString.equals("Aug")) { month = 7; }
				else if (monthString.equals("Sep")) { month = 8; }
				else if (monthString.equals("Oct")) { month = 9; }
				else if (monthString.equals("Nov")) { month = 10; }
				else if (monthString.equals("Dec")) { month = 11; }
				int day = scan.nextInt();
				int numDaysBooked = scan.nextInt();
				int numSingles = 0;
				int numDoubles = 0;
				int numTriples = 0;
				if (scan.hasNext("single")) {
					scan.next();
					numSingles = scan.nextInt();
				}
				if (scan.hasNext("double")) {
					scan.next();
					numDoubles = scan.nextInt();
				}
				if (scan.hasNext("triple")) {
					scan.next();
					numTriples = scan.nextInt();
				}
				hotelBookingSystem.addBooking(bookingId, month, day, numDaysBooked, numSingles, numDoubles, numTriples, true);
			} else if (firstWord.equals("Change")) {
				int bookingId = scan.nextInt();
				String monthString = scan.next();
				int month = -1;
				if (monthString.equals("Jan")) { month = 0; }
				else if (monthString.equals("Feb")) { month = 1; }
				else if (monthString.equals("Mar")) { month = 2; }
				else if (monthString.equals("Apr")) { month = 3; }
				else if (monthString.equals("May")) { month = 4; }
				else if (monthString.equals("Jun")) { month = 5; }
				else if (monthString.equals("Jul")) { month = 6; }
				else if (monthString.equals("Aug")) { month = 7; }
				else if (monthString.equals("Sep")) { month = 8; }
				else if (monthString.equals("Oct")) { month = 9; }
				else if (monthString.equals("Nov")) { month = 10; }
				else if (monthString.equals("Dec")) { month = 11; }
				int day = scan.nextInt();
				int numDaysBooked = scan.nextInt();
				int numSingles = 0;
				int numDoubles = 0;
				int numTriples = 0;
				if (scan.hasNext("single")) {
					scan.next();
					numSingles = scan.nextInt();
				}
				if (scan.hasNext("double")) {
					scan.next();
					numDoubles = scan.nextInt();
				}
				if (scan.hasNext("triple")) {
					scan.next();
					numTriples = scan.nextInt();
				}
				hotelBookingSystem.changeBooking(bookingId, month, day, numDaysBooked, numSingles, numDoubles, numTriples);
			} else if (firstWord.equals("Cancel")) {
				int bookingId = scan.nextInt();
				hotelBookingSystem.cancelBooking(bookingId, true);
			} else if (firstWord.equals("Print")) {
				String hotelName = scan.next();
				for (Hotel a: hotelBookingSystem.hotels) {
					if (a.getName().equals(hotelName)) {
						a.printHotel();
					}
				} 
			}
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} finally {
		if (scan != null) { scan.close(); }
	}
}
/**
* Takes in a hotel name and creates a new Hotel with that name
* @param name The name of the hotel to be created
*/
public void addHotel(String name) {
	Hotel newHotel = new Hotel(name);
	hotels.add(newHotel);
}
/**
 * Takes in a request to change a booking and tries to fulfill it.
 * @precondition bookingId >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @precondition numSingles >= 0
 * @precondition numDoubles >= 0
 * @precondition numTriples >= 0
 * @param bookingId	The ID of the booking
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 * @param numSingles The number of single rooms
 * @param numDoubles The number of double rooms
 * @param numTriples The number of triple rooms
 * @return True if making a valid change to an existing booking
 */
public boolean changeBooking(int bookingId, int month, int day, int numDaysBooked, int numSingles, int numDoubles, int numTriples) {
	boolean changeSuccess = false;
	String hotelName = null;
	if(cancelBooking(bookingId, false)) {
		for (Hotel a: hotels) {
			if (a.checkVacancy(numSingles, numDoubles, numTriples, month, day, numDaysBooked)) {
				hotelName = addBooking(bookingId, month, day, numDaysBooked, numSingles, numDoubles, numTriples, false);
				break;
			}
		}
		if (hotelName != null) {
			changeSuccess = true;
		}
	}
	if (changeSuccess == true) {
		System.out.print("Change " + bookingId + " " + hotelName + " ");
		for (Hotel a: hotels) {
			if (a.getName().equals(hotelName)) {
			a.getBookedRooms(bookingId);
			System.out.println();
			}	
		}
	} else {
		System.out.println("Change rejected");
	}
	return changeSuccess;
}
/**
 * Takes in a request to add a booking and tries to fulfill it.
 * @precondition bookingId >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @precondition numSingles >= 0
 * @precondition numDoubles >= 0
 * @precondition numTriples >= 0
 * @param bookingId	The ID of the booking
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 * @param numSingles The number of single rooms
 * @param numDoubles The number of double rooms
 * @param numTriples The number of triple rooms
 * @param shouldPrint Prints if making a booking, doesn't if changing one.
 * @return True if making a valid booking
 */
public String addBooking(int bookingId, int month, int day, int numDaysBooked, int numSingles, int numDoubles, int numTriples, boolean shouldPrint) {
	String hotelBooked = null;
	boolean bookingMade = false;
	boolean bookingExists = false;
	for (Booking b: bookings) {
		if (b.getBookingId() == bookingId) {
			bookingExists = true;
		}
	}
	if (!bookingExists) {
	for (Hotel a: hotels) {
		if (a.checkVacancy(numSingles, numDoubles, numTriples, month, day, numDaysBooked)) {
			if (a.bookRooms(numSingles, numDoubles, numTriples, bookingId, month, day, numDaysBooked)) {
				bookingMade = true;
				hotelBooked = a.getName();
				if (shouldPrint == true) {
					System.out.print("Booking " + bookingId + " " + hotelBooked + " ");
					a.getBookedRooms(bookingId);
					System.out.println();
				}
				break;
			}
		}
	}
	if (bookingMade == true) {
		Booking newBooking = new Booking();
		newBooking.setBookingId(bookingId);
		newBooking.setMonth(month);
		newBooking.setDay(day);
		newBooking.setNumDaysBooked(numDaysBooked);
		newBooking.setNumSingles(numSingles);
		newBooking.setNumDoubles(numDoubles);
		newBooking.setNumTriples(numTriples);
		newBooking.setHotelName(hotelBooked);
		bookings.add(newBooking);
	} else {
		if (shouldPrint == true) {
			System.out.println("Booking rejected");
		}
	}
	} else {
		if (shouldPrint == true) {
			System.out.println("Booking rejected");
		}
	}
	return hotelBooked;
}
/**
 * Takes in a request to add a booking and tries to fulfill it.
 * @precondition bookingId >= 0
 * @param bookingId	The ID of the booking
 * @param shouldPrint Prints if canceling a booking, doesn't if changing one.
 * @return True if making a valid cancellation
 */
public boolean cancelBooking(int bookingId, boolean shouldPrint) {
	boolean cancelSuccess = false;
	for (Booking a: bookings) {
		if (a.getBookingId() == bookingId) {
			String bookedHotel = a.getHotelName();
			int month = a.getMonth();
			int day = a.getDay();
			int numDaysBooked = a.getNumDaysBooked();
			for (Hotel b: hotels) {
				if (b.getName().equals(bookedHotel)) {
					b.deAllocateRooms(month, day, numDaysBooked, bookingId);
					break;
				}
			}
			bookings.remove(a);
			cancelSuccess = true;
			break;
		}
	}
	if (cancelSuccess == true && shouldPrint == true){
		System.out.println("Cancel " + bookingId);
	} else {
		if (shouldPrint == true) {
			System.out.println("Cancel rejected");
		}
	}
	return cancelSuccess;
}
}
