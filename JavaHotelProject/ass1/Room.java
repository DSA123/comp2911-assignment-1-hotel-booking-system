/*
 * This class is responsible for holding information about each room and
 * maintaining vacancy of the rooms via an array.
 */
public class Room {
	private int roomNumber;
	private int size;
	private Booking[] vacancy = new Booking[365];
public Room(int roomNumber, int size) {
	this.roomNumber = roomNumber;
	this.size = size;
	for (int i = 0; i<vacancy.length; i++) {
		this.vacancy[i] = new Booking();
	}
	for(Booking a: vacancy) {
		a.setBookingId(-1); // fill with -1
	}
}
/**
 * Checks the vacancy of a specific room.
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 * @return True if room has enough vacancies to fulfill request
 */
public boolean checkRoomVacancy(int month, int day, int numDaysBooked) {
	boolean vacancyCheck = false;
	int index = convertDateToIndex(month,day);
	int counter = 0;
		while  (counter < numDaysBooked) {
			int checkDate = this.vacancy[index++].getBookingId();
			counter++;
			if (checkDate != -1) {
				return false;
			}
		}
		vacancyCheck = true;
	return vacancyCheck;
}
/**
 * Sets the vacancy of a specific room.
 * @precondition bookingId >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @param bookingId The ID of the booking.
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 */
public void setRoomVacancy(int bookingId, int month, int day, int numDaysBooked) {
	int index = convertDateToIndex(month, day);
	int counter = 0;
	while (counter < numDaysBooked){
		this.vacancy[index++].setBookingId(bookingId);
		counter++;
	}
}
/**
 * Deallocates a room by finding any instances of a specific bookingId 
 * within a room and setting it to vacant (-1).
 * @precondition bookingId >= 0
 * @precondition month >= 0
 * @precondition day >= 0
 * @precondition numDaysBooked >= 0
 * @param bookingId The ID of the booking.
 * @param month The month being booked
 * @param day The initial day of booking
 * @param numDaysBooked The total number of days being booked
 */
public void deallocateRoom(int month, int day, int numDaysBooked, int bookingId) {
	int index = convertDateToIndex(month, day);
	int counter = 0;
	while (counter < numDaysBooked) {
		if (this.vacancy[index].getBookingId() == bookingId) {
			this.vacancy[index++].setBookingId(-1); // -1 denotes a non booked room date.
			counter++;
		} else {
			break;
		}
	}
}
/**
 * Searches for a specific bookingId within a room.
 * @precondition bookingId >= 0
 * @param bookingId The ID of the booking.
 */
public void findBooking(int bookingId) {
	int i = 0;
	while (i < vacancy.length) {
		if (this.vacancy[i].getBookingId() == bookingId) {
			System.out.print(this.roomNumber + " ");
			break;
		} else {
			i++;
		}
	}
}
/**
 * Prints out the room number and specific information about
 * the days booked (if any).
 */
public void printRoomVacancy() {
	System.out.print(this.roomNumber + " ");
	int counter = 0;
	int i = 0;
	int j = 0;
	while (i<vacancy.length) {
		int currentId = this.vacancy[i].getBookingId();
		int prevId = this.vacancy[j].getBookingId();
		if (i == 0) {
			if (currentId != -1) {
				System.out.print("Jan 1 ");
				counter++;
				i++;
			} else {
				i++;
			}
		} else {
			if (currentId != -1 && currentId == prevId) {
				counter++;
				i++;
				j++;
			} else if (currentId == -1) {		
				if (counter > 0) { System.out.print(counter + " "); }
				counter = 0;
				i++;
				j++;
			} else if (currentId != prevId) {
				if (counter > 0) { System.out.print(counter + " "); }
				counter = 0;
				int month = convertIndexToMonth(i);
				int day = convertIndexToDay(i);
				switch(month) {
				case 0: System.out.print("Jan " + day + " "); // jan
						break;
				case 1: System.out.print("Feb " + day + " "); // feb
						break;
				case 2: System.out.print("Mar " + day + " "); // march
						break;
				case 3: System.out.print("Apr " + day + " "); // april
						break;
				case 4: System.out.print("May " + day + " "); // may
						break;
				case 5: System.out.print("Jun " + day + " "); // june
						break;
				case 6: System.out.print("Jul " + day + " "); // july
						break;
				case 7: System.out.print("Aug " + day + " "); // aug
						break;
				case 8: System.out.print("Sep " + day + " "); // sep
						break;
				case 9: System.out.print("Oct " + day + " "); // oct
						break;
				case 10: System.out.print("Nov " + day + " "); // nov
						break;
				case 11: System.out.print("Dec " + day + " "); // dec
						break;
				}
				counter++;
				i++;
				j++;
			}
		}
	}
}
/**
 * Converts a month and day into an index which can be used for array
 * referencing.
 * @precondition month >= 0
 * @precondition day >= 0
 * @param month The month being booked.
 * @param day The initial day of booking
 * @return An index if exception not thrown.
 * @exception IllegalArgumentException If there is an illegal argument or invalid input
 */
private int convertDateToIndex(int month, int day) {
	int index = -1;
	switch(month) {
		case 0: index = day - 1; // jan
				break;
		case 1: index = 30 + day; // feb
				break;
		case 2: index = 58 + day; // march
				break;
		case 3: index = 89 + day; // april
				break;
		case 4: index = 119 + day; // may
				break;
		case 5: index = 150 + day; // june
				break;
		case 6: index = 180 + day; // july
				break;
		case 7: index = 211 + day; // aug
				break;
		case 8: index = 242 + day; // sep
				break;
		case 9: index = 272 + day; // oct
				break;
		case 10: index = 303 + day; // nov
				break;
		case 11: index = 333 + day; // dec
				break;
	}
	if (index == -1) {
		 throw new IllegalArgumentException();
	}
	return index;
}
/**
 * Converts an index into a month.
 * @precondition index >= 0 && index <= 364
 * @param index The index value being converted into a month.
 * @return A month value if given a valid index.
 */
private int convertIndexToMonth(int index) {
	int month = -1;
	if (index >= 0 && index <= 30) { month = 0; } // jan
	else if (index >= 31 && index <= 58) { month = 1; } // feb
	else if (index >= 59 && index <= 89) { month = 2; } // march
	else if (index >= 90 && index <= 119){ month = 3; } // april
	else if (index >= 120 && index <= 150){ month = 4; } // may
	else if (index >= 151 && index <= 180){ month = 5; }// june
	else if (index >= 181 && index <= 211){ month = 6; }// july
	else if (index >= 212 && index <= 242){ month = 7; }// aug
	else if (index >= 243 && index <= 272){ month = 8; }// sep
	else if (index >= 273 && index <= 303){ month = 9; }// oct
	else if (index >= 304 && index <= 333){ month = 10; }// nov
	else if (index >= 334 && index <= 364){ month = 11; }// dec
	else {
		//invalid month
	}
	return month;
}
/**
 * Converts an index into a day value.
 * @precondition index >= 0 && index <= 364
 * @param index The index value being converted into a day value.
 * @return A day value if given a valid index.
 */
private int convertIndexToDay(int index) {
	int day = -1;
	if (index >= 0 && index <= 30) { day = index + 1; } // jan
	else if (index >= 31 && index <= 58) { day = index - 30; } // feb
	else if (index >= 59 && index <= 89) { day = index - 58; } // march
	else if (index >= 90 && index <= 119){ day = index - 89; } // april
	else if (index >= 120 && index <= 150){ day = index - 119; } // may
	else if (index >= 151 && index <= 180){ day = index - 150; }// june
	else if (index >= 181 && index <= 211){ day = index - 180; }// july
	else if (index >= 212 && index <= 242){ day = index - 211; }// aug
	else if (index >= 243 && index <= 272){ day = index - 242; }// sep
	else if (index >= 273 && index <= 303){ day = index - 272; }// oct
	else if (index >= 304 && index <= 333){ day = index - 303; }// nov
	else if (index >= 334 && index <= 364){ day = index - 333; }// dec
	else {
		//invalid day
	}
	return day;
}
public int getSize() {
	return size;
}
public int getRoomNumber() {
	return roomNumber;
}
}
